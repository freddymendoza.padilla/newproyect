<?php 
include_once('../../config/init_db.php');

DB::$error_handler = false;
DB::$throw_exception_on_error = true;

 class Product
 {
 	public static function validar_cod($code_prod){
 		try{
 			$validar = DB::query("SELECT * FROM tabla_producto WHERE code_prod = $code_prod");
 			 if (!empty($validar)) {
 			 	$data['error'] = true;
                $data['mensaje'] = 'Codigo ya existe';
 			 }else{
 			 	$data['error'] = false;
                $data['mensaje'] = 'Seguir';
 			 }
 		}catch(MeekroDBException $e) {
            echo "Error: " . $e->getMessage() . "<br>\n";
            echo "SQL Query: " . $e->getQuery() . "<br>\n";
			$data['error'] = true;
            $data['mensaje'] = 'Error, no se logro guardar la información';

        }
        return $data;

 	}


 	public static function mdlInsert($info){
 	  	extract($info);
 	  
        try {
        	  $validar = Product::validar_cod($code_prod);
        	  if ($validar['error']) {
        	  	  return $validar;
        	  }

              $insert = DB::query(" INSERT INTO tabla_producto(
													code_prod,
													name_prod,
													ctry_prod,
													price_prod,
													peso_prod,
													stock_prod                                             
                                                     )
                                                     VALUES(
                                                     '{$code_prod}',
                                                     '{$name_prod}',
                                                     '{$category_prod}',
                                                      {$price_prod},
                                                       $peso_kg,
                                                      {$stock_prod}
                                                            )");
            $data['error'] = false;
            $data['mensaje'] = 'Información creada correctamente';
        } catch(MeekroDBException $e) {
            echo "Error: " . $e->getMessage() . "<br>\n";
            echo "SQL Query: " . $e->getQuery() . "<br>\n";
			$data['error'] = true;
            $data['mensaje'] = 'Error, no se logro guardar la información';
        }
        return $data;
    }

    public static function mdlListar(){

    	try{
    		$listar = db::query("SELECT * FROM tabla_producto");
    	}catch(MeekroDBException $e){
    		echo "Error:". $e->getMessage() . "<br>\n";
    		echo "SQL Query:". $e->getMessage() . "<br>\n";
    	}
    	return $listar;
    }

    public static function consProdId($id_produt){
   
    	try{
    		$listar = db::query("SELECT * FROM tabla_producto where id = $id_produt");
    	}catch(MeekroDBException $e){
    		echo "Error:". $e->getMessage() . "<br>\n";
    		echo "SQL Query:". $e->getMessage() . "<br>\n";
    	}
    	return $listar;
    }

    public static function editProducto($p){
   		extract($p);
    	try{
    		
        	  $codigoactual = DB::queryFirstRow("SELECT * FROM tabla_producto WHERE id = $id");
        	 if ($codigoactual['code_prod'] != $code_prod) {
        	 	  $validar = Product::validar_cod($code_prod);
	        	  if ($validar['error']) {
	        	  	  return $validar;
	        	  }	
        	 }
    		 DB::query(" UPDATE
										    tabla_producto
										SET
										    code_prod  = '{$code_prod}',
										    name_prod  = '{$name_prod}',
										    ctry_prod  = '{$category_prod}',
										    price_prod = {$price_prod},
										    peso_prod  =   $peso_kg,
										    stock_prod = {$stock_prod},
										    date_edition = NOW()
										  
										WHERE
										    id = $id ");
    		$data['error'] = false;
            $data['mensaje'] = 'Información editada correctamente';
    	}catch(MeekroDBException $e){
    		echo "Error:". $e->getMessage() . "<br>\n";
    		echo "SQL Query:". $e->getMessage() . "<br>\n";
    		$data['error'] = true;
            $data['mensaje'] = 'Error al editar producto';
    	}
    	return $data;
    }

      public static function deletProducto($delete_id){
   		
    	try{
    		  $deletprod = DB::query("DELETE FROM tabla_producto WHERE  id = $delete_id");
    	}catch(MeekroDBException $e){
    		echo "Error:". $e->getMessage() . "<br>\n";
    		echo "SQL Query:". $e->getMessage() . "<br>\n";
    	}
    	return $deletprod;
    }
    

 }
