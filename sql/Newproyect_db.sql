-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 03-06-2021 a las 05:04:03
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `Newproyect_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_producto`
--

CREATE TABLE `tabla_producto` (
  `id` int(10) NOT NULL,
  `code_prod` varchar(50) NOT NULL,
  `name_prod` varchar(50) NOT NULL,
  `ctry_prod` varchar(50) NOT NULL,
  `price_prod` int(11) NOT NULL,
  `peso_prod` int(11) NOT NULL,
  `stock_prod` int(10) NOT NULL,
  `date_creation` date NOT NULL DEFAULT current_timestamp(),
  `date_edition` datetime DEFAULT NULL,
  `date_vent` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tabla_producto`
--

INSERT INTO `tabla_producto` (`id`, `code_prod`, `name_prod`, `ctry_prod`, `price_prod`, `peso_prod`, `stock_prod`, `date_creation`, `date_edition`, `date_vent`) VALUES
(1, '0001', ' Avena quaker', 'cereales', 20000, 30, 50, '2021-06-02', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tabla_producto`
--
ALTER TABLE `tabla_producto`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `libro` (`code_prod`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tabla_producto`
--
ALTER TABLE `tabla_producto`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
